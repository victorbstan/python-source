import opencv
#this is important for capturing/displaying images
from opencv import highgui 
import pygame
import sys

import ImageFilter
import ImageOps

camera = highgui.cvCreateCameraCapture(0)
def get_image():
    img = highgui.cvQueryFrame(camera)
    # Add the line below if you need it (Ubuntu 8.04+)
    img = opencv.cvGetMat(img)
    # convert Ipl image to PIL image
    return opencv.adaptors.Ipl2PIL(img) 

fps = 15.0
pygame.init()
window = pygame.display.set_mode((640,480))
pygame.display.set_caption("Camera-feed")
screen = pygame.display.get_surface()

#### my stuff

def process(image):
	# print image
	
	# do something with the image
	# image = image.filter(ImageFilter.EDGE_ENHANCE_MORE)
	# image = image.filter(ImageFilter.CONTOUR)
	# image = image.filter(ImageFilter.FIND_EDGES)
	# image = image.filter(ImageFilter.EMBOSS)
	# image = image.filter(ImageFilter.SMOOTH)
	# image = image.filter(ImageFilter.SMOOTH_MORE)
	# image = image.filter(ImageFilter.SHARPEN)
	# image = image.filter(ImageFilter.MinFilter())
	# image = image.filter(ImageFilter.ModeFilter())
	# image = image.filter(ImageFilter.MaxFilter(9))


	# image = ImageOps.grayscale(image) # wrong format output 
	# image = ImageOps.posterize(image, 8)
	image = ImageOps.solarize(image, threshold=128)
	image = ImageOps.invert(image)
	image = ImageOps.equalize(image)

	return image

#### end my stuff

# start the display and application

while True:
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            sys.exit(0)
    img = process(get_image())
    pg_img = pygame.image.frombuffer(img.tostring(), img.size, img.mode)
    screen.blit(pg_img, (0,0))
    pygame.display.flip()
    pygame.time.delay(int(1000 * 1.0/fps))
    # pygame.time.delay(int(1.0/fps))

    


